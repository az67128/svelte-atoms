### 0.0.27
22.05.2020
- DropZone component

### 0.0.26
15.05.2020
- notification warn fix

### 0.0.25
27.04.2020
- Dropdown not visible after resize

### 0.0.24
23.04.2020
- Fileupload Icon fix
- Fileupload cursor pointer
- Component Progress added

### 0.0.23
14.04.2020
- Alerts added as export from index

### 0.0.22
11.04.2020
- Icon now accept objects

### 0.0.21
10.04.2020
- Cell, Row, Container got class prop
- Alert component

### 0.0.20
09.04.2020
- Button isLoading disabling button
- New icons
- Icons stored as js
- Demo navigation changed

### 0.0.19
06.04.2020
- addNotification method for notifications added
- Status colors for notifications added
- Button warning status added

### 0.0.18
04.04.2020
- index.js for import from root
- Button disabled fix
- Button display changed to inline-block
