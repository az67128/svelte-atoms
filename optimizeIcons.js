const fs = require("fs");
const path = require("path");
const SVGO = require("svgo");

const ICONDIR = path.join("src", "components", "icons");
const EXCLUDE = ["index.js"];

/* (!) Optimizes only two cases: 
*   - icon is a path-string
*   - icon is array where elements are path-string or object with path property.
*/
const svgo = new SVGO();
(async () => {
  fs.readdirSync(ICONDIR).forEach(async (file) => {
    if (EXCLUDE.includes(file)) return;
    
    const filepath = path.join(ICONDIR, file);

    const getIconObject = new Function(
      fs
        .readFileSync(filepath, "utf-8")
        .replace(/export[\s]+default/i, "return ")
    );
    
    let iconObject = getIconObject();

    let sizeBefore = 0;
    let sizeAfter = 0;

    if (!Array.isArray(iconObject)) iconObject = [iconObject];
    
    for (let i in iconObject) {
      if(isPath(iconObject[i])){
        sizeBefore += iconObject[i].length;
        iconObject[i] = await optimize(iconObject[i]);
        sizeAfter += iconObject[i].length;
      }else if(isObjWithProp(iconObject[i],'path')){
        sizeBefore += iconObject[i].path.length;
        iconObject[i].path = await optimize(iconObject[i].path);
        sizeAfter += iconObject[i].path.length;
      }
    }

    let content = iconObject.map(p =>{
      if(isObjWithProp(p,'path')) return JSON.stringify(p).replace(/"([^"]+)":/g, '$1:'); 
      if(isPath(p)) return `"${p}"`; 
    }).join(',');

    content = `export default ${(iconObject.length === 1) ? content : `[${content}]`};`
    fs.writeFileSync(filepath, content);

    console.log(`${file}: ${sizeBefore}->${sizeAfter}`);
  });
})();

async function optimize(path) {
  const result = await svgo.optimize(`<svg><path d="${path}"/></svg>`);
  return result.data.match(/d="([^"]+)"/i)[1];
}

function isObjWithProp(obj,prop){
  return (typeof obj === 'object' && obj[prop]);
}

function isPath(str){
  return (typeof str === 'string' && str.startsWith('M'));
}